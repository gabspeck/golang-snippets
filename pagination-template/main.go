package main

import (
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"math"
)

type Result struct {
	Title string
}

type PageLink struct {
	Number int
	Href   string
}

type Pagination struct {
	CurrentNumber int
	First         *PageLink
	Previous      *PageLink
	Links         []PageLink
	Next          *PageLink
	Last          *PageLink
}

type Page struct {
	Results []Result
	*Pagination
}

const resultsPerPage = 10
const totalResults = 23
const pageParam = "p"

var tpl = template.Must(template.ParseGlob("*.html"))

func main() {

	http.HandleFunc("/", handleResults)
	http.ListenAndServe(":"+os.Getenv("PORT"), nil)
}

func handleResults(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	pageNumber, _ := strconv.Atoi(q.Get("p"))
	if pageNumber < 1 {
		pageNumber = 1
	}

	page := &Page{
		Results:    getResults(pageNumber),
		Pagination: getPagination(r.URL.Path, q, pageNumber, totalResults, resultsPerPage),
	}

	if err := tpl.ExecuteTemplate(w, "results", page); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func getHref(path string, params url.Values, page int) string {
	params.Set(pageParam, strconv.Itoa(page))
	return fmt.Sprintf("%s?%s", path, params.Encode())
}

func getPagination(path string, query url.Values, current, totalResults, resultsPerPage int) *Pagination {
	totalPages := int(math.Ceil(float64(totalResults)/float64(resultsPerPage)))
	if totalPages < 2 {
		return nil
	}
	p := &Pagination{
		CurrentNumber: current,
	}
	previousNumber, nextNumber := current - 1, current + 1
	if current > 1 {
		p.First = &PageLink{Number: 1, Href: getHref(path, query, 1)}
		p.Previous = &PageLink{Number: previousNumber, Href: getHref(path, query, previousNumber)}
	}
	firstPageLink := current - min(2, current - 1)
	lastPageLink := current + min(2, totalPages- current)
	for i := firstPageLink; i <= lastPageLink; i++{
		pl := PageLink{
			Number: i,
			Href: getHref(path, query, i),
		}
		p.Links = append(p.Links, pl)
	}
	if current < totalPages {
		p.Last = &PageLink{Number: totalPages, Href: getHref(path, query, totalPages)}
		p.Next = &PageLink{Number: nextNumber, Href: getHref(path, query, nextNumber)}
	}

	return p
}

func min(a, b int) int{
	if a < b {
		return a
	}
	return b
}

func getResults(page int) []Result {
	var results []Result
	start := ((page - 1) * resultsPerPage) + 1
	end := resultsPerPage * page
	for i := start; i <= end; i++ {
		res := Result{
			fmt.Sprintf("Resultado %d", i),
		}
		results = append(results, res)
	}
	return results
}
